const admin = require('firebase-admin');
const config = require('./index');

var serviceAccount = require("../fb.json");

var db =admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://sign-fee-collection-system-default-rtdb.asia-southeast1.firebasedatabase.app/"
});

module.exports=db;
