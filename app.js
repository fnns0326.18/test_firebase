var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('passport');
const helmet = require("helmet");
const rateLimit = require('express-rate-limit');
const core = require('cors');



//require config
const config = require("./config/index");




// connect to mongo
const mongoose = require('mongoose');
mongoose.connect(config.MONGODB_URI,{
   useNewUrlParser: true, //for delete warning from console
   useUnifiedTopology: true, //for delete warning from console
  // useCreateIndex: true, //for delete warning from console
  // useFindAndModify: false, //for delete warning from console
 
}).then(() => {
  console.log("Database Connection is Ready...")
}).catch((err) => {
  console.log(err)
});



var indexRouter = require('./routes/index');
const userRouter = require('./routes/userRoutes');
const studentRouter = require('./routes/studentRoute');



const app = express();
app.use(core());

// block req
const limiter = rateLimit({
	windowMs: 15 *  1000, // 10 s
	max: 1000, // Limit each IP to 100 requests per `window` (here, per 15 minutes)
	standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
	legacyHeaders: false, // Disable the `X-RateLimit-*` headers
})

// Apply the rate limiting middleware to all requests


app.use(limiter)


app.use(helmet());



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json({
  limit:'50mb'
}));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// intit passoort
app.use(passport.initialize());




app.use('/', indexRouter);
app.use('/api/users',userRouter);
app.use('/api/student',studentRouter);





// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

//acess can get public file
app.use("/public", express.static(path.resolve(__dirname + '/public')));

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
