const User = require('../Models/usersModel');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
//const { config } = require('dotenv');
const config = require('../config/index');
const { validationResult } = require('express-validator');
const { findByIdAndUpdate } = require('../Models/usersModel');
const firebase =require('../config/db');

// show all data
exports.index = async (req, res, next) => {

    try {

        const users = await User.find().sort({
            _id: -1
        });
        
        const userPhotoDomain = await users.map((user, index) => {
            return {
                id: user._id,
                f_uid: user.f_uid,
                fname: user.fname,
                lname: user.lname,
                gender: user.gender,
                email: user.email,
                status: user.status,
              //  photo: 'http://localhost:3000/images/' + user.photo,
                photo: `${req.protocol}://${req.get('host')}/images/` + user.photo,
                address: user.address

            }
        })

        res.status(200).json({
            data: userPhotoDomain
        })

    } catch (error) {
        res.status(500).json({
            message: error.message
        })

    }
}

// show data by id

exports.showByID = async (req, res, next) => {
    try {
        const {
            id
        } = req.params;




        const userID = await User.findById(id);
        if (!userID) {
            res.status(404).json({
                message: 'Not Found!'
            })
        } else {
            res.status(200).json({
                data: userID
            })
        }



    } catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}
// insert data to tb_users
exports.insert = async (req, res, next) => {
    try {
        const {
            fname,
            lname,
            gender,
            phone,
            email,
            password,
            photo,
            status,
            province,
            distict,
            village
        } = req.body;

        // validation
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            res.status(422).json({
                errors:errors.array()
            })
        }




        //const {fname,lname,gender}= req.body;
        //hash password
        const salt = await bcrypt.genSalt(10);
        const passwordHash = await bcrypt.hash(password, salt);

        // cheack email
        const exitEmail = await User.findOne({
            email: email
        })
        if (!exitEmail) {

            // save data to firebase
            let fuser = firebase.auth().createUser({
                email:email,
                password:password
            })
           let f_uid=(await fuser).uid


            //save data to mongodb

            let user = new User({
                f_uid:f_uid,
                fname: fname,
                lname: lname,
                gender: gender,
                phone: phone,
                email: email,
                password: passwordHash,
                status: status,
                address: {
                    province: province,
                    distict: distict,
                    village: village
                }

            });

            await user.save();



            res.status(201).json({
                message: 'Register successssss!'

            })

        } else {
            res.status(400).json({
                message: 'This Email has exit',
            });
        }



    } catch (error) {
        res.status(500).json({
            message: error.message
        })

    }
}

/// delete user
exports.Delete = async (req, res, next) => {
    try {
        const {
            id
        } = req.params;

        const userID = await User.findById(id);
        if (!userID) {
            res.status(404).json({
                message: 'Not Found!'
            })
        } else {
            firebase.auth().deleteUser(userID.f_uid)
            await User.deleteOne({
                _id: id
            });
            res.status(200).json({
                message: "delete successss!"
            })
        }


    } catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

// update user info
exports.update = async (req, res, next) => {
    try {
        const {
            id
        } = req.params;
        const {
            fname,
            lname,
            gender,
            phone,
            email,
            password,
            photo,
            status,
            province,
            distict,
            village
        } = req.body;

        const userID = await User.findById(id);
        if (!userID) {
            res.status(404).json({
                message: 'Not Found!'
            })
        } else {
            const salt = await bcrypt.genSalt(10);
            const passwordHash = await bcrypt.hash(password, salt);
            await User.findOneAndUpdate(id, {
                fname: fname,
                lname: lname,
                gender: gender,
                phone: phone,
                email: email,
                password: passwordHash,
                status: status,
                address: {
                    province: province,
                    distict: distict,
                    village: village
                }
            });
            res.status(200).json({
                message: "Update successss!"
            })
        }
    } catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
}

// login
//let user1 = new User();
exports.login = async (req, res, next) => {
    try {
        const {
            email,
            password
        } = req.body;
        // cheack email
        const user = await User.findOne({
            email: email
        })
        if (!user) {
            res.status(404).json({
                message: 'Not Found !'
            })
        }


        // cheack password

        const pass = await bcrypt.compare(password, user.password)
        if (!pass) {
            res.status(404).json({
                message: 'password wrong !'
            })
        }


        // create token 
        const token = await jwt.sign({
                id: user._id,
                fullName: user.fname +' '+ user.lname,
                role: user.status
            },
            config.JWT_SECRET, {
                expiresIn: '7 days' // Expire token time
            }
        );
        // decode token
        const expires_in = jwt.decode(token);


        res.status(200).json({
            access_token: token,
            expires_in: expires_in.exp, //expire day with decode to timestamp
            token_type: 'Bearer', //for tell fontend for send data to header
        })
    } catch (error) {
        res.status(500).json({
            message: error.message

        })
    }
}

// get profile 
exports.me = async( req, res, next)=>{
   try {
    const {_id,fname,lname,status, email} = req.user;

    res.status(200).json({
        user:{
            id:_id,
            fullname : fname +" "+ lname,
            email:email,
            role:status
            
        }
    })
   } catch (error) {
    res.status(500).json({
        error:error.message
    })
   }
}

// change password
exports.changepass = async (req, res, next)=>{
    try {
        const {_id,password} = req.user;
        const {password_old, password_new}=req.body;

        // cheack old password
        const pass = await bcrypt.compare(password_old, password)
        if (!pass) {
            res.status(404).json({
                message: 'password old is wrong !',
               
          
            })
        }else{
            // update password
            await User.findOneAndUpdate(_id,{
                password:password_new
            })

            res.status(200).json({
                message: 'change password success!',
               
          
            })
        }

        
    } catch (error) {
        res.status(500).json({
            message:error.message
        })
    }
}

// forgot password
exports.forgotPass= async (req, res, next)=>{
    try {
        const {email, password} = req.body;
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then((userCredential) => {
            const user = userCredential.user;
            res.status(200).json({
                data:user
             })
            })

         
    } catch (error) {
        res.status(500).json({
            error: error.message
        })
    }
}