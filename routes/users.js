var express = require('express');
var router = express.Router();
const bcrypt = require('bcryptjs');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

//compare password in systems with bcrypt.js
schema.methods.checkPassword = async function (password) {
  const isValid = await bcrypt.compare(password, this.password);
  return isValid; //return true or false
};

module.exports = router;
