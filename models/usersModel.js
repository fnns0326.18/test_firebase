const { string } = require('joi');
const mongoose= require('mongoose');

const schema = new mongoose.Schema({
f_uid:{type:"string", trim:true},
fname:{type:"string", trim:true},
lname:{type:"string", trim:true},
gender:{type:"string", trim:true},
phone:{type:"string", trim:true},
email:{type:"string",requred:true, trim:true},
password:{type:"string",requred:true, trim:true},
photo: {type:String, default:'nopic.png'},
address:{
   province:{type:"string",requred:true, trim:true},
   distict:{type:"string",requred:true, trim:true},
   village:{type:"string",requred:true, trim:true}
},
status:{type:"string",default:'user', trim:true},
//usertype:{type:"string", trim:true}
},{
    timestamps:true,
    collection: 'tb_users'
}
);

const User = mongoose.model('tb_user',schema);
module.exports = User;