require("dotenv").config();
const assert = require('assert');

const {PORT, HOST}= process.env;

assert(PORT, 'PORT is required');
assert(HOST, 'HOST is required');

module.exports={
    MONGODB_URI: process.env.MONGODB_URI,
    JWT_SECRET: process.env.JWT_SECRET,
    PORT: process.env.PORT,
    

    // firebase
    firebaseConfig : {
    apiKey: process.env.API_KEY,
    authDomain:process.env.AUTH_DOMAIN,
    databaseURL:process.env.DATABASE_URL,
    projectId:process.env.PROJECT_ID,
    storageBucket:process.env.STORAGE_BUCKET,
    messagingSenderId:process.env.MESSAGING_SENDER_ID,
    appId:process.env.APP_ID
    }
};