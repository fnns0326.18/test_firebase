const express = require('express');
const {body} = require('express-validator');
const router = express.Router();
const user = require('../controllers/usersController');
const passportJWT = require('../middleware/passortJWT');
const cheackAdmin = require('../middleware/chackAdmin');
const User = require('../Models/usersModel');


router.get('/all',[passportJWT.isLogin],user.index);
router.get('/:id',[
    passportJWT.isLogin
  //,cheackAdmin.isAdmin
],user.showByID);

router.post('/',[
    body('email').not().isEmpty().withMessage('please input email').isEmail().withMessage('email is not correct!'),
    body('password').not().isEmpty().withMessage('please input password').isLength({min:6}).withMessage('password is less than 5 char')
],user.insert);


router.delete('/:id',user.Delete);
router.put('/:id',user.update);
router.post('/login',user.login);

// get profile 
router.get('/pro/me',[passportJWT.isLogin],user.me);

//change password
router.post('/chagepass',[passportJWT.isLogin],user.changepass);


module.exports = router;