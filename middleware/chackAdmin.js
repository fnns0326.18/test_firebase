module.exports.isAdmin = (req, res, next)=>{
    const {status} = req.user;

    if(status === 'admin'){
        next();
    }else{
        return res.status(403).json({
            message:'admin only!'
        });
    }
}